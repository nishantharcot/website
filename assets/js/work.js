(
  document.onreadystatechange = () => {
    if (document.readyState === 'complete') {
      const model = {

      };
      const view = {
        init : () => {
          view.render();
        },
        render : () => {

        }
      };
      const controller = {
        init : () => {
          view.init();
        }
      }
      controller.init();
    }
  }
)();